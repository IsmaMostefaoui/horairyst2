# Horairyst 2.0

Cette application est le fruit du Projet de Master à l'Université de Mons. Elle permet la création et l'amélioration d'horaire de présentation de travaux lors de sessions parrallèles.
Ce logiciel a été conçu sur base de **Horairyst**, un ancien Projet de Master à l'université créé par Duncan De Weireld.
**Horairyst** a été conçu dans l'unique but d'optimiser en ligne de commande via programmation linéaire (scip en l'occurence) des horaires pré-créés au format *.csv* ou *.xls*.
Une interface web était aussi disponible afin de visualiser l'horaire ainsi optimisé.

L'objectif de ce projet est avant-tout la création de *A* à *Z* d'horaire en sessions parrallèles, appuyé d'outil visuel afin de permettre un réarrangement de la solution linéaire proposée par **Horairyst**.

## Installation:

* Avoir une version de python 3.x d'installée ainsi que pip

```bash
sudo apt install -y python3-pip
```

* Avoir installé PyQt5

```bash
pip3 install PyQt5
```

* (si l'installation en fonctionne pas via pip, essayer via apt install)

```bash
sudo apt-get install python3-pyqt5
```

## Lancement

Exécuter la ligne de commande suivante pour python 3.x:

```bash
python3 src/main_app.py
```

## Création d'un nouvel horaire

Au lancement, cette fenêtre devrait apparaître:

![Empty Home](/ressources/images/empty_home.png)

Pour créer un nouvel horaire il suffit de cliquer sur "Fichier" en haut à gauche, puis sur "Nouveau".

Lorsqu'un nouvel horaire un créé, cette fenêtre devrait se présenter:

![Created Home](/ressources/images/horayrist_new_home.png)

Pour ajouter un nouveau slot, il suffit de double-cliquer sur une des case de l'horaire ou bien de faire clique droit puis "Ajouter/Modifier une présentation". Cette fenêtre devrait alors s'ouvrir:

![Slot Creation](/ressources/images/slot_creation.png)

Aucun professeur ne s'affiche car il n'en existe pas encore. Pour en ajouter, allez dans "Edition" puis "Ajouter des professeurs".

Pour ajouter des professeurs, ils suffit d'écrire leurs noms et prénoms en suivant cette règle: **Prénom Nom;Prénom Nom;** sans espace après les **;**. 

![Complete teachers](/ressources/images/complete_new_teacher.png)

Vous pouvez invereser *Nom* et *Prénom*, mais assurez vous de suivre la même logique tout le temps.

Une fois plusieurs professeurs ajoutés, ces derniers apparaitront comme choix possible pour chaque création de case. Double-cliquez à nouveau sur une case pour le constater:

![Complete slot creation](/ressources/images/add_teacher.png)

## Ouverture d'un horaire

Pour ouvrir un horaire, allez dans "File" puis "Ouvrir un horaire". 

Les horaires de tests sont au format `.csv` et se trouvent dans: `/ressources/schedule/csv`

Les horaires que vous créerez plus tard seront au format `.hor` et se trouveront dans `/ressources/schedule/hor`

De la même façon qu'ils sont créés, les horaires peuvent être modifiés et déplacés.

## Utilisation de Horairyst

Une fois un horaire créé ou ouvert, les cases peuvent se déplacer via le *Drag and Drop*. Des cases en surbillance rouge signifient qu'un conflit à lieu entre ces cases.
De plus, les cases peuvent être mise dans la liste d'attente afin de simplifier la manipulation de grands horaires.

![Conflict Schedule](/ressources/images/conflict_waiting.png)

Pour supprimer une présentation, cliquez-droit sur la présentation puis "Supprimer la présentation".

Il est aussi possible de supprimer et/ou d'ajouter une session. Pour se faire, cliquez-droit sur la session puis "Supprimer une session" ou "Ajouter une session". Cette dernière est aussi modifiable
via le double-clique ou le menu contextuel du clique-droit.

Enfin, vous pouvez modifier les créneaux horaires disponible ainsi que leurs pattern. Allez dans "Edition" puis "Editer les périodes". Cette fenêtre devrait se présenter.

Ici, pouvez modifier l'heure de début et de fin de votre horaire, mais vous pouvez aussi
modifier la durée des présentations. Un maximum de 1h et un minimum de 15 minutes doivent cependant être respectés.

![Conflict Schedule](/ressources/images/period_modif.png)

## Sauvegarde de fichiers

Dans **Horairyst 2.0**, il existe deux types de fichiers:

- Les fichiers *.csv* tels qu'ils l'étaient dans **Horairyst**. Ces fichiers de sauvegarde ne garde en mémoire que les étudiants présent à l'horaire, les membres du jury pour 
    chaque étudiant ainsi que le type de présentation. En aucun il ne garde la position des modifications qui y sont apportées.
    C'est pourquoi il ne faut en aucun cas sauvegarde une modification d'horaire sous le format *.csv*, **Horairyst 2.0** ne le supporte pas.
 - Les fichiers *.hor*, la nouveauté de **Horairyst 2.0**. Ces fichiers de sauvegarde garde en mémoire la position de chaque présenation ainsi que les modifications apportées. 
 Cependant la liste d'attente ne représente qu'une aide. Elle n'est pas sauvegardée dans le fichier *.hor*.
 
 Pour sauvegarder, allez dans "Fichier" puis "Sauvegarder". Et voilà, votre horaire est sauvé !
 
 ## Exportation en LateX
 
 Pour exporter votre horaire en Latex, allez dans "Fichier" puis "Exporter en LateX". Vous aurez à remplir les modalités de délibération.
 Une fois ces modalités remplies, créez votre nouveau fichier latex.