import pickle

from horairyst.parsers import csvParser
from horairyst.problem.problem import Problem


class ScheduleWriter(object):

    def __init__(self):
        pass

    @staticmethod
    def writeHor(problem, output='test.hor', new=False):
        with open(output, 'wb') as f:
            if new:
                problem = csvParser.parse('src/controller/newFileLoaded.csv')
            if problem.matrix_sol is None:
                problem.matrix_sol = problem.getSolutionAsJSONMatrix()
            print(f'saving to {output}')
            pickle.dump(problem.matrix_sol, f)

    @staticmethod
    def parseHor(it='test.hor'):
        with open(it, 'rb') as f:
            matrix_sol = pickle.load(f)
            if isinstance(matrix_sol, dict):
                return Problem.fromJsonMatrix(matrix_sol)

    @staticmethod
    def writeLatex(problem, output='test.tex', param=None):
        if param is None:
            param = []
        with open(output, 'w') as f:
            if param:
                latex = problem.getSolutionAsLatex(param[0], param[1], param[2], param[3], param[4], param[5])
            else:
                latex = problem.getSolutionAsLatex()
            f.write(latex)
