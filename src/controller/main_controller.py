import sys
from copy import deepcopy
from random import randint

from PyQt5 import Qt
from PyQt5.QtCore import QObject, QThread, pyqtSlot
from datetime import timedelta

from PyQt5.QtWidgets import QMessageBox

from controller.ScheduleWriter import ScheduleWriter
from horairyst.parsers import csvParser, xlsParser
from horairyst.problem.problem import Problem
from horairyst.solvers.scip import Solver

all_current_teacher = []


class MainController(QObject):

    @property
    def solver(self):
        return self._solver

    def __init__(self, model):
        super().__init__()

        self._model = model
        self._solver = Solver(model)
        self.writer = ScheduleWriter()

        self.empty_matrix_slot = self.create_matrix_slot("", [], [])

        self.mul_score = ['linear']*Problem.class_of_score

        self.curr_thread = None

    def test_model_problem(self):
        print(f'Problem is: {self._model.problem}')

    def create_matrix_slot(self, student, directors, rapporters):
        if student == "" and directors == [] and rapporters == []:
            return {'student': '', 'teachers': [], 'validity': {'value': True, 'reasons': []}}
        else:
            teachers = []
            for d in directors:
                teachers.append({'name': d, 'role': 'D'})
            for r in rapporters:
                teachers.append({'name': r, 'role': 'R'})
            return {'student': student, 'teachers': teachers, 'validity': {'value': True, 'reasons': []}}

    def get_filename(self, pathfile):
        dir_list = pathfile.split("/")
        filename = dir_list[-1]
        return filename

    def refresh(self):
        try:
            # recharge du problème pour accéder à la validité de ce dernier
            self._model.problem = Problem.fromJsonMatrix(self._model.problem.matrix_sol)
        except IndexError:
            diag = QMessageBox()
            diag.setText("Une erreur est survenue, toutes vos modifications non sauvegardées ont été perdue.")
            diag.setInformativeText("Veuillez redémarrer l'application.")
            diag.setIcon(QMessageBox.Critical)
            diag.exec_()
        if self.curr_thread is not None and self.curr_thread.isRunning():
            self.curr_thread.quit()
            self.curr_thread.wait()
        self._solver.solution_changed.emit(self._model.problem)

    @pyqtSlot(Problem)
    def on_solution_changed(self, problem, mul=None):
        if mul is None:
            mul = deepcopy(self.mul_score)
        else:
            self.mul_score = deepcopy(mul)
        self._model.problem.computeScore(mul)

    @pyqtSlot()
    def optimize_button_pressed(self):
        if self.curr_thread is not None and self.curr_thread.isRunning():
            self.curr_thread.quit()
            self.curr_thread.wait()
        if self.curr_thread is None:
            self.curr_thread = QThread()
            self._solver.moveToThread(self.curr_thread)
            self.curr_thread.started.connect(self._solver.solve)
        if self._model.problem.matrix_sol is None:
            self._model.problem.matrix_sol = self._model.problem.getSolutionAsJSONMatrix()
        self._model.problem = Problem.fromJsonMatrix(self._model.problem.matrix_sol)  # just to notify the view
        self.curr_thread.start()

    def file_new(self, schedule_pathfile):
        filename = self.get_filename(schedule_pathfile)
        if filename.split('.')[-1] != 'hor':
            schedule_pathfile += '.hor'
        self.writer.writeHor(self._model.problem, schedule_pathfile, new=True)
        self.files_open(schedule_pathfile, False)

    def files_open(self, schedule_pathfile, solve=True):
        is_hor = False
        self.mul_score = ['linear']*Problem.class_of_score
        if self.curr_thread is not None and self.curr_thread.isRunning():
            self.curr_thread.quit()
            self.curr_thread.wait()
        if self.curr_thread is None:
            self.curr_thread = QThread()
            self._solver.moveToThread(self.curr_thread)
            self.curr_thread.started.connect(self._solver.solve)
        schedule_pathfile = self._model.check_args(schedule_pathfile)
        schedule_filename = self.get_filename(schedule_pathfile)
        self._model.schedule_name = schedule_filename
        ext = "." + schedule_filename.split(".")[-1]
        if ext in csvParser.getHandledExtensions():
            self._model.problem = csvParser.parse(schedule_pathfile)
        elif ext in xlsParser.getHandledExtensions():
            self._model.problem = xlsParser.parse(schedule_pathfile)
        elif ext == '.hor':
            solve = False
            is_hor = True
            self._model.problem = self.writer.parseHor(schedule_pathfile)
        if self._model.problem is None:
            sys.exit(-1)
        all_current_teacher.clear()
        all_current_teacher.extend(self._model.problem.directors)
        if solve:
            self.curr_thread.start()
        else:
            self._solver.solution_changed.emit(self._model.problem)
        return is_hor

    def save_problem(self, schedule_pathfile):
        try:
            filename = self.get_filename(schedule_pathfile)
            ext = "." + filename.split(".")[-1]
            filename = filename.split('.')[0]
            if ext == '.csv':
                schedule_pathfile = 'ressources/schedule/hor/'+filename+'.hor'
            elif ext != '.hor':
                schedule_pathfile += '.hor'
            self.writer.writeHor(self._model.problem, schedule_pathfile)
        except FileNotFoundError:
            return False
        return True

    def write_latex(self, pathfile, param):
        filename = self.get_filename(pathfile)
        ext = "." + filename.split(".")[-1]
        if ext != '.tex':
            pathfile += '.tex'
        param = list(map(str, param))
        print(self._model.problem)
        self.writer.writeLatex(self._model.problem, pathfile, param)

    @pyqtSlot(list)
    def change_slot_pos(self, values):
        pos = values[:len(values) - 1]
        if self._model.problem.matrix_sol is None:
            self._model.problem.matrix_sol = self._model.problem.getSolutionAsJSONMatrix()
        matrix = self._model.problem.matrix_sol['matrix']
        if pos[0] < 0:
            if pos[2] >= 0:
                matrix[pos[2]][pos[3]] = self.empty_matrix_slot
        elif pos[2] < 0:
            student = values[len(values) - 1].property('student')
            if values[len(values) - 1].property('type') == "Projet":
                student = "L:" + student
            else:
                student = "M:" + student
            directors = values[len(values) - 1].property('director')
            rapporters = values[len(values) - 1].property('rapporter')
            matrix[pos[0]][pos[1]] = self.create_matrix_slot(student, directors, rapporters)
        else:
            tmp = matrix[pos[0]][pos[1]]
            matrix[pos[0]][pos[1]] = matrix[pos[2]][pos[3]]
            matrix[pos[2]][pos[3]] = tmp
        self._model.problem.matrix_sol['matrix'] = matrix
        self.refresh()

    @pyqtSlot(list)
    def created_slot(self, values):
        if self._model.problem.matrix_sol is None:
            self._model.problem.matrix_sol = self._model.problem.getSolutionAsJSONMatrix()
        matrix = self._model.problem.matrix_sol['matrix']
        pos = (values[0], values[1])
        if values[len(values) - 1] == "Projet":
            student = "L:" + values[2]
        else:
            student = "M:" + values[2]
        matrix[pos[0]][pos[1]] = self.create_matrix_slot(student, values[3], values[4])
        self._model.problem.matrix_sol['matrix'] = matrix
        self.refresh()

    @pyqtSlot(list)
    def deleted_slot(self, values):
        if self._model.problem.matrix_sol is None:
            self._model.problem.matrix_sol = self._model.problem.getSolutionAsJSONMatrix()
        matrix = self._model.problem.matrix_sol['matrix']
        pos = (values[0], values[1])
        matrix[pos[0]][pos[1]] = self.empty_matrix_slot
        self._model.problem.matrix_sol['matrix'] = matrix
        self.refresh()

    @pyqtSlot(list)
    def added_session(self, value):
        if self._model.problem.matrix_sol is None:
            self._model.problem.matrix_sol = self._model.problem.getSolutionAsJSONMatrix()
        index = self._model.problem.matrix_sol['sessions'].index(value[0])
        self._model.problem.matrix_sol['sessions'].insert(index + 1, "Session #" + str(randint(1000, 9999)))
        matrix = self._model.problem.matrix_sol['matrix']
        for e in matrix:
            e.insert(index + 1, self.empty_matrix_slot)
        self._model.problem.matrix_sol['matrix'] = matrix
        self.refresh()

    @pyqtSlot(list)
    def changed_session(self, value):
        if self._model.problem.matrix_sol is None:
            self._model.problem.matrix_sol = self._model.problem.getSolutionAsJSONMatrix()
        for i, sess in enumerate(value[0]):
            index = self._model.problem.matrix_sol['sessions'].index(sess)
            self._model.problem.matrix_sol['sessions'][index] = value[1][i]
        self.refresh()

    def deleted_session(self, value):
        if self._model.problem.matrix_sol is None:
            self._model.problem.matrix_sol = self._model.problem.getSolutionAsJSONMatrix()
        index = self._model.problem.matrix_sol['sessions'].index(value[0])
        self._model.problem.matrix_sol['sessions'].pop(index)
        matrix = self._model.problem.matrix_sol['matrix']
        for e in matrix:
            e.pop(index)
        self._model.problem.matrix_sol['matrix'] = matrix
        self.refresh()

    def edit_period(self, hour_range):
        # calcul du nombre de slot rempli par ligne
        number_of_slot = []
        if self._model.problem.matrix_sol is None:
            self._model.problem.matrix_sol = self._model.problem.getSolutionAsJSONMatrix()
        matrix = self._model.problem.matrix_sol['matrix']
        for slot in matrix:
            x = 0
            for sub_slot in slot:
                if sub_slot['student'] != "":
                    x += 1
            number_of_slot.append(x)

        # recupération des informations d'heure
        source = hour_range[0]
        target = hour_range[1]

        target_begin = timedelta(hours=target[0].hour(), minutes=target[0].minute())  # 9:00
        target_end = timedelta(hours=target[1].hour(), minutes=target[1].minute())  # 13:00

        step = timedelta(hours=int(target[2].split(':')[0]), minutes=int(target[2].split(':')[1]))  # 00:45

        # construction de chaque nouvelle période
        indicator = target_begin
        per = []
        while indicator <= target_end:
            str_per = str(indicator)[:-3].replace(':', 'h')
            per.append(str_per)
            indicator += step

        # test de la compatibilité des nouvelles périodes sur base des slots déjà occupés
        if len(per) < len(self._model.problem.periods):
            diff_line = len(self._model.problem.periods) - len(per)
            first_line_empty, last_lines_empty = 0, 0
            for i in range(diff_line):
                if number_of_slot[-(i + 1)] == 0:
                    last_lines_empty += 1
                else:
                    break
            for i in range(diff_line):
                if number_of_slot[i] == 0:
                    first_line_empty += 1
                else:
                    break
            if first_line_empty + last_lines_empty < diff_line:
                return -1
            cut_matrix = []
            if last_lines_empty:
                if first_line_empty:
                    for i in range(len(per)):
                        cut_matrix.append(matrix[i + first_line_empty])
                else:
                    for i in range(len(per)):
                        cut_matrix.append(matrix[i])
            elif first_line_empty:
                for i in range(len(per)):
                    cut_matrix.append(matrix[i + first_line_empty])
            self._model.problem.matrix_sol['matrix'] = cut_matrix
            self._model.problem.matrix_sol['periods'] = per
        elif len(per) > len(self._model.problem.periods):
            new_matrix = []
            for i in range(len(per)):
                if i >= len(matrix):
                    new_matrix.append([self.empty_matrix_slot] * len(self._model.problem.periods))
                else:
                    new_matrix.append(matrix[i])
        self._model.problem.matrix_sol['periods'] = per
        self.refresh()
