from PyQt5.QtCore import QObject, pyqtSignal
import horairyst
import horairyst.problem.constraint as constraint
from horairyst.problem.problem import Problem
from importlib import reload


class ModelLoader(QObject):

    problem_changed = pyqtSignal(Problem)
    schedule_name_changed = pyqtSignal(str)

    @property
    def problem(self):
        return self._problem

    @problem.setter
    def problem(self, value):
        constraint.clearConstraints()
        reload(horairyst.mods.strongConstraints)
        reload(horairyst.mods.weakConstraints)
        self._problem = value
        self.problem_changed.emit(value)

    @property
    def schedule_name(self):
        return self._schedule_name

    @schedule_name.setter
    def schedule_name(self, value):
        self._schedule_name = value
        self.schedule_name_changed.emit(value)

    def __init__(self):
        super().__init__()
        self.importMods()
        self._problem = None
        self._schedule_name = "None"

    @staticmethod
    def importMods():
        # clear old imports
        constraint.clearConstraints()
        # Import mods

        import horairyst.mods.strongConstraints
        import horairyst.mods.weakConstraints

        """
        for module in os.listdir("src/horairyst/mods"):
            if module == '__init__.py' or module[-3:] != '.py':
                continue
            print(f'module: {module[:-3]}')
            __import__("horairyst.mods", locals(), globals(), [module[:-3]])
            
        del module
        """
    @staticmethod
    def check_args(_args):  # TODO
        return _args
