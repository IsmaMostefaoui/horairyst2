import subprocess
from PyQt5.QtCore import QObject, pyqtSignal, QThread

from horairyst.problem.problem import Problem


class Solver(QObject):

    solverStarted = pyqtSignal()
    solution_changed = pyqtSignal(Problem)
    error_pop = pyqtSignal(str)
    infeasible_error = pyqtSignal(str)

    def __init__(self, model):
        super().__init__()
        self._model = model
        self.outfile = "/tmp/horairyst_output"

    def _runCommand(self, cmd):
        subprocess.getoutput(cmd)
        res = ""
        file = open(self.outfile, "r")
        for line in file:
            res += line
        file.close()
        return res

    def _createBatch(self, filePath):
        text = "read " + filePath + "\n"
        text += "optimize\n"
        text += "write solution " + self.outfile + "\n"
        text += "quit"

        file = open(self.outfile, "w")
        file.write(text)
        file.flush()
        file.close()
        return self.outfile

    def _getSolverOutput(self, filePath):
        batchPath = self._createBatch(filePath)
        print("Getting sol...")
        x = self._runCommand("./scip -b " + batchPath)
        print("Sol found.")
        return x

    def _extractSolution(self, output):
        lines = output.split("\n")
        res = {"status": lines[0].split(":")[1].strip()}
        if res["status"] == "infeasible":
            self.infeasible_error.emit("InfeasibleError - Le problème courant est infaisable")
            return
        res["value"] = float(lines[1].split(":")[1].strip())
        res["solution"] = []
        for i in range(2, len(lines) - 1):
            ln = lines[i].split("\t")[0].split(" ")
            res["solution"].append((ln[0], float(ln[-2])))
        print("-" * 80)
        return res

    def solve(self, filePath="/tmp/pb.lp"):
        # print(f'I work on problem: {self._model.problem} inside a thread')
        self._model.problem.resetSolution()

        f = open(filePath, "w")
        toWrite = self._model.problem.write()
        if toWrite == "":
            f.close()
            return -1
        f.write(toWrite)
        f.close()
        sol = self._extractSolution(self._getSolverOutput(filePath))
        if self._model.problem.setSolution(sol) == -1:
            # print('Emit an error')
            self.error_pop.emit("Index error - Ouverture d'un fichier plus petit que le précédent")
            return -1
        else:
            self.solverStarted.emit()
            self.solution_changed.emit(self._model.problem)
            return sol
