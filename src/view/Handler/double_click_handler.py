from PyQt5.QtCore import QTimer, pyqtSignal, pyqtSlot


class DoubleClickHandler:

    left_clicked = pyqtSignal(int)

    def __init__(self):
        self.timer = QTimer()
        self.timer.setInterval(250)
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.timeout)
        self.left_click_count = 0
        self.left_clicked.connect(self.on_double_clicked)

    def timeout(self):
        if self.left_click_count > 1:
            self.left_clicked.emit(self.left_click_count)
        self.left_click_count = 0

