from datetime import timedelta

from PyQt5 import QtCore
from PyQt5.QtCore import QDir, QTime, pyqtSlot, Qt
from PyQt5.QtWidgets import QFileDialog, QDialog, QMessageBox
from controller import main_controller
from view.UI_Files import latex_edit_ui, period_edit, edition_score_ui, teacher_edit_ui


class OnClickHandler:

    def __init__(self, model, window, controller):
        self._model = model
        self.window = window
        self._main_controller = controller

    def show_items(self):
        self.window.optimize_button.show()
        self.window.add_waiting_list()

    def on_clicked_new_schedule(self):
        self.window.confirm_save()
        dlg = QFileDialog(self.window)
        dlg.setFixedSize(100, 100)
        dlg.setFileMode(QFileDialog.AnyFile)
        myDir = QDir.current()
        myDir.cd("ressources/schedule/")
        dlg.setDirectory(myDir)
        dlg.setParent(self.window)
        file = dlg.getSaveFileName(None, "Create a new file", "", "")
        if len(file[0]) > 0:
            self.window.current_problem_pathfile = file[0]
            self._main_controller.file_new(file[0])
            self.show_items()

    def on_clicked_open_schedule(self):
        self.window.confirm_save()
        dlg = QFileDialog(self.window)
        dlg.setFileMode(QFileDialog.AnyFile)
        myDir = QDir.current()
        myDir.cd("ressources/schedule/")
        dlg.setDirectory(myDir)
        dlg.setParent(self.window)
        # dlg.setFilter("csv or xls files (*.csv)")
        filename = ""
        if dlg.exec_():
            filename = dlg.selectedFiles()
        if len(filename) > 0:
            self.window.current_problem_pathfile = filename[0]
            is_hor = self._main_controller.files_open(filename[0])
            if is_hor:
                self.window.sh.on_solution_changed(self._model.problem)
            else:
                self.window.loading()
            self.show_items()

    def on_optimize_button_pressed(self):
        if self._model.problem is None or len(self._model.problem.students) == 0:
            self.window.no_problem_open()
            return
        if not self.window.end_loading:
            return
        self.window.loading()
        self._main_controller.optimize_button_pressed()

    def on_clicked_save_problem(self):
        if self._model.problem is None:
            self.window.no_problem_open()
            return
        no_err = self._main_controller.save_problem(self.window.current_problem_pathfile)
        if no_err:
            self.window.save_state = True
            self.window.correct_save()
        else:
            self.window.error_save()

    def on_clicked_save_as_problem(self):
        if self._model.problem is None:
            self.window.no_problem_open()
            return
        dlg = QFileDialog(self.window)
        dlg.setFileMode(QFileDialog.AnyFile)
        myDir = QDir.current()
        myDir.cd("ressources/schedule/")
        dlg.setDirectory(myDir)
        dlg.setParent(self.window)
        file = dlg.getSaveFileName(None, "Create a new file", "", "")
        if len(file[0]) > 0:
            self.window.current_problem_pathfile = file[0]
            no_err = self._main_controller.save_problem(self.window.current_problem_pathfile)
            if no_err:
                self.window.save_state = True
                self.window.correct_save()
            else:
                self.window.error_save()

    def on_clicked_latex(self):
        if self._model.problem is None:
            self.window.no_problem_open()
            return
        self.window.confirm_save()
        diag = QDialog(self.window)
        latex_edit = latex_edit_ui.Ui_Dialog()
        latex_edit.setupUi(diag)
        if diag.exec_():
            step = latex_edit.step.time()
            step = step.minute()
            begin_delib = latex_edit.begin_delib.time()
            begin_delib = str(begin_delib.hour()) + 'h' + (
                str(begin_delib.minute()) if begin_delib.minute() != 0 else '00')
            end_delib = latex_edit.end_delib.time()
            end_delib = str(end_delib.hour()) + 'h' + (str(end_delib.minute()) if end_delib.minute() != 0 else '00')
            begin_master = latex_edit.begin_master.time()
            begin_master = str(begin_master.hour()) + 'h' + (
                str(begin_master.minute()) if begin_master.minute() != 0 else '00')
            begin_procla_time = latex_edit.begin_procla.time()
            begin_procla = str(begin_procla_time.hour()) + 'h' + (
                str(begin_procla_time.minute()) if begin_procla_time.minute() != 0 else '00')
            end_procla_time = latex_edit.end_procla.time()
            end_procla = str(end_procla_time.hour()) + 'h' + (
                str(end_procla_time.minute()) if end_procla_time.minute() != 0 else '00')
        else:
            return

        dlg = QFileDialog(self.window)
        dlg.setFixedSize(100, 100)
        dlg.setFileMode(QFileDialog.AnyFile)
        myDir = QDir.current()
        myDir.cd("ressources/schedule/latex")
        dlg.setDirectory(myDir)
        dlg.setParent(self.window)
        file = dlg.getSaveFileName(None, "Create a new file", "", "")
        if len(file[0]) > 0:
            self._main_controller.write_latex(file[0],
                                              [step, begin_delib, end_delib, begin_master, begin_procla, end_procla])

    def on_clicked_edit_period(self):
        if self._model.problem is None:
            self.window.no_problem_open()
            return
        diag = QDialog(self.window)
        period_diag = period_edit.Ui_Dialog()
        period_diag.setupUi(diag)

        # period computation
        period = self._model.problem.periods
        begin_hour = list(map(int, period[0].split('h')))
        second_hour = list(map(int, period[1].split('h')))
        end_hour = list(map(int, period[-1].split('h')))
        period_diag.time_begin.setTime(QtCore.QTime(begin_hour[0], begin_hour[1]))
        period_diag.time_end.setTime(QtCore.QTime(end_hour[0], end_hour[1]))
        delta1 = timedelta(hours=begin_hour[0], minutes=begin_hour[1])
        delta2 = timedelta(hours=second_hour[0], minutes=second_hour[1])
        step = str(delta2 - delta1)[:-3]
        step_minute = (delta2 - delta1).seconds / 60
        period_diag.period_slider.setMaximum(60)
        period_diag.period_slider.setMinimum(15)
        period_diag.period_step.setText(step)
        period_diag.period_slider.setValue(step_minute)

        format_hour = lambda value: str(timedelta(minutes=value))[:-3]

        @pyqtSlot(int)
        def value_changed(value):
            period_diag.period_step.setText(format_hour(value))

        period_diag.period_slider.valueChanged.connect(value_changed)
        if diag.exec_():
            self.window.save_state = False
            error = self._main_controller.edit_period(
                [(QTime(begin_hour[0], begin_hour[1]), QTime(end_hour[0], end_hour[1]), step),
                 (period_diag.time_begin.time(), period_diag.time_end.time(),
                  format_hour(period_diag.period_slider.value()))])
            if error == -1:
                self.window.on_error_popped("Impossible de créer les périodes. Des présentations seraient supprimées.")

    def on_clicked_score(self):
        if self._model.problem is None:
            self.window.no_problem_open()
            return
        diag = QDialog(self.window)
        score_diag = edition_score_ui.Ui_Dialog()
        score_diag.setupUi(diag)
        score_diag.mul_box.setCurrentText(format_text_score(self._main_controller.mul_score[0]))
        score_diag.mul_box_alt.setCurrentText(format_text_score(self._main_controller.mul_score[1]))
        # score computation
        if diag.exec_():
            mul = score_diag.mul_box.currentText()
            mul = self.format_mul(mul)
            mul_alt = score_diag.mul_box_alt.currentText()
            mul_alt = self.format_mul(mul_alt)
            mul = [mul, mul_alt]
            self._main_controller.on_solution_changed(None, mul)

    def format_mul(self, mul):
        if mul == 'Linéaire':
            return 'linear'
        elif mul == 'Exponentielle':
            return 'exp'
        elif mul == 'Logarithmique':
            return 'log'

    def on_teacher_add(self):
        if self._model.problem is None:
            self.window.no_problem_open()
        else:
            diag = QDialog(self.window)
            teacher_edit = teacher_edit_ui.Ui_Dialog()
            teacher_edit.setupUi(diag)
            teacher_edit.teacher_edit.setText(';'.join(main_controller.all_current_teacher))
            if diag.exec_():
                self.window.save_state = False
                teacher = teacher_edit.teacher_edit.toPlainText().split(";")
                for t in teacher:
                    if any(char.isdigit() for char in t):
                        msg = QMessageBox()
                        msg.setIcon(QMessageBox.Critical)
                        msg.setText("Un professeur ne peut contenir des chiffres.")
                        msg.setInformativeText("Veuillez entrer des noms de professeurs valide.")
                        msg.setWindowFlags(Qt.WindowStaysOnTopHint)
                        msg.exec_()
                        return
                main_controller.all_current_teacher = teacher


def format_text_score(text):
    if text == 'linear':
        return 'Linéaire'
    elif text == 'exp':
        return 'Exponentielle'
    elif text == 'log':
        return 'Logarithmique'
    elif text == 'holes':
        return 'Nombre de trous par professeur'
    elif text == 'sessions':
        return 'Nombre de changement de sessions'
