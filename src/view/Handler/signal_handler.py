from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout, QDialogButtonBox
from view.Widget.slot_label import QEmpty, QSession, QHour, QSlot


class OnSignalHandler:

    def __init__(self, model, window, controller):
        self._model = model
        self.window = window
        self._main_controller = controller

    def on_schedule_name_changed(self, text):
        to_replace = self.window.ui.schedule_name_label.text()
        split_list = to_replace.split(":")
        static_label = split_list[0] + ": "
        self.window.ui.schedule_name_label.setText(static_label + text)

    def on_session_deleted(self, value):
        self.window.save_state = False
        msg = QDialog(self.window)
        msg.setFixedSize(500, 100)
        msg.setWindowTitle("Attention")
        label = QLabel(msg)
        label.setWordWrap(True)
        label.setText("En supprimant cette session, vous risquez de perdre toutes les présentations" +
                      " actuellement associées à cette sessions.\nEtes-vous sûr de vouloir continuer ?")
        vbox = QVBoxLayout(msg)
        vbox.addWidget(label)
        buttonBox = QDialogButtonBox(msg)
        buttonBox.setOrientation(Qt.Horizontal)
        buttonBox.setStandardButtons(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
        buttonBox.accepted.connect(msg.accept)
        buttonBox.rejected.connect(msg.reject)
        vbox.addWidget(buttonBox)
        if msg.exec_():
            self._main_controller.deleted_session(value)

    def on_problem_changed(self, problem):
        self.window.save_state = False
        self.window.reset_view()
        self.window.ui.gridLayout.addWidget(QEmpty(), 0, 0, 1, 1)
        for i, sess in enumerate(problem.sessions):
            widget = QSession(sess)
            widget.session_changed.connect(self._main_controller.changed_session)
            widget.session_deleted.connect(self.on_session_deleted)
            widget.session_added.connect(self._main_controller.added_session)
            self.window.ui.gridLayout.addWidget(widget, 0, i + 1, 1, 1)
        for i, per in enumerate(problem.periods):
            self.window.ui.gridLayout.addWidget(QHour(per), i + 1, 0, 1, 1)
        for i, _ in enumerate(problem.periods):
            for j, _ in enumerate(problem.sessions):
                widget = QSlot(i, j, "", "", "")
                widget.slot_changed.connect(self._main_controller.change_slot_pos)
                widget.slot_created.connect(self._main_controller.created_slot)
                widget.slot_deleted.connect(self._main_controller.deleted_slot)
                self.window.ui.gridLayout.addWidget(widget, i + 1, j + 1, 1, 1)

    def on_solution_changed(self, problem):
        self.window.save_state = False
        self.window.end_loading = True
        signal = problem.score_changed
        signal.connect(self.window.ui.scoreNumber.display)
        signal = problem.score_changed_alt
        signal.connect(self.window.ui.scoreNumberAlt.display)
        if problem.json_sol is None:
            problem.json_sol = problem.getSolutionAsJson()
        for slot in problem.json_sol['slots']:
            column = problem.sessions.index(slot['room']) + 1
            row = problem.periods.index(slot['hour']) + 1
            student = slot['student'].split(':')
            directors = []
            rapporters = []
            for p in slot['teachers']:
                if p['role'] == 'D':
                    directors.append(p['name'])
                else:
                    rapporters.append(p['name'])

            item = self.window.ui.gridLayout.itemAt(
                self.window.get_grid_index(row - 1, column - 1, len(problem.periods),
                                           len(problem.sessions)))
            widget = item.widget()
            if isinstance(widget, QSlot):
                widget.setStudent(student[1])
                widget.setDirectors(directors)
                widget.setRapporters(rapporters)
                if student[0] != 'L':
                    widget.setSubject("Mémoire")
                widget.refresh()
        self.checkValiditySlot(self._model.problem.validity)

    def checkValiditySlot(self, validity):
        """
        Vérifie que le changement de solution n'entraine pas de conflit horaire dans le problème.
        Si oui, parcourt tous les conflits et modifie la validité de chaque slot afin de visuellement notifier le conflit
        :param validity:
        :return: None
        """
        if not validity[0]:
            if not self.window.not_valid_slots:
                for slot in self.window.not_valid_slots:
                    slot.empty_reasons()
                    slot.validity = True
                    slot.refresh()
                self.window.not_valid_slots = []
            for notValidSlot in validity[4]:
                pos_slot = notValidSlot[2]
                grid_pos_slot = self.window.get_grid_index(pos_slot[1], pos_slot[0], len(self._model.problem.periods),
                                                           len(self._model.problem.sessions))
                reason = notValidSlot[0]
                slot = self.window.ui.gridLayout.itemAt(grid_pos_slot).widget()
                slot.validity = False
                slot.reasons.append(reason)
                self.window.not_valid_slots.append(slot)
                slot.refresh()

        if validity[0]:
            if not self.window.not_valid_slots:
                for slot in self.window.not_valid_slots:
                    slot.empty_reasons()
                    slot.validity = True
                    slot.refresh()
            self.window.not_valid_slots = []
