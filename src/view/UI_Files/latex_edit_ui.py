# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ressources/ui/latex_edit.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 237)
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.a1 = QtWidgets.QLabel(Dialog)
        self.a1.setObjectName("a1")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.a1)
        self.a2 = QtWidgets.QLabel(Dialog)
        self.a2.setObjectName("a2")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.a2)
        self.a3 = QtWidgets.QLabel(Dialog)
        self.a3.setObjectName("a3")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.a3)
        self.a4 = QtWidgets.QLabel(Dialog)
        self.a4.setObjectName("a4")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.a4)
        self.a5 = QtWidgets.QLabel(Dialog)
        self.a5.setObjectName("a5")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.a5)
        self.begin_delib = QtWidgets.QTimeEdit(Dialog)
        self.begin_delib.setObjectName("begin_delib")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.begin_delib)
        self.end_delib = QtWidgets.QTimeEdit(Dialog)
        self.end_delib.setObjectName("end_delib")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.end_delib)
        self.begin_master = QtWidgets.QTimeEdit(Dialog)
        self.begin_master.setObjectName("begin_master")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.begin_master)
        self.begin_procla = QtWidgets.QTimeEdit(Dialog)
        self.begin_procla.setObjectName("begin_procla")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.begin_procla)
        self.end_procla = QtWidgets.QTimeEdit(Dialog)
        self.end_procla.setObjectName("end_procla")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.end_procla)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.step = QtWidgets.QTimeEdit(Dialog)
        self.step.setObjectName("step")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.step)
        self.gridLayout.addLayout(self.formLayout, 0, 0, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 1, 0, 1, 1)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Edition - Latex"))
        self.a1.setText(_translate("Dialog", "Heure de début des délibérations:"))
        self.a2.setText(_translate("Dialog", "Heure de fin des délibérations:"))
        self.a3.setText(_translate("Dialog", "Heure de début de délibération Master:"))
        self.a4.setText(_translate("Dialog", "Heure de début des proclamations:"))
        self.a5.setText(_translate("Dialog", "Heure de fin des proclamations:"))
        self.label.setText(_translate("Dialog", "Durée des présentations:"))

