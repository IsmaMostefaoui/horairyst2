# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ressources/ui/edition_score.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(493, 166)
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 1, 0, 1, 1)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setBold(True)
        font.setItalic(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.mul_box = QtWidgets.QComboBox(Dialog)
        self.mul_box.setObjectName("mul_box")
        self.mul_box.addItem("")
        self.mul_box.addItem("")
        self.mul_box.addItem("")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.mul_box)
        self.label_3 = QtWidgets.QLabel(Dialog)
        font = QtGui.QFont()
        font.setBold(True)
        font.setItalic(True)
        font.setUnderline(True)
        font.setWeight(75)
        font.setKerning(True)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.mul_box_alt = QtWidgets.QComboBox(Dialog)
        self.mul_box_alt.setObjectName("mul_box_alt")
        self.mul_box_alt.addItem("")
        self.mul_box_alt.addItem("")
        self.mul_box_alt.addItem("")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.mul_box_alt)
        self.gridLayout.addLayout(self.formLayout, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Nombre de trous :"))
        self.label_2.setText(_translate("Dialog", "Fonction d\'application :"))
        self.mul_box.setItemText(0, _translate("Dialog", "Linéaire"))
        self.mul_box.setItemText(1, _translate("Dialog", "Exponentielle"))
        self.mul_box.setItemText(2, _translate("Dialog", "Logarithmique"))
        self.label_3.setText(_translate("Dialog", "Nombre de changement de sessions :"))
        self.label_4.setText(_translate("Dialog", "Fonction d\'application :"))
        self.mul_box_alt.setItemText(0, _translate("Dialog", "Linéaire"))
        self.mul_box_alt.setItemText(1, _translate("Dialog", "Exponentielle"))
        self.mul_box_alt.setItemText(2, _translate("Dialog", "Logarithmique"))

