from PyQt5.QtWidgets import QMainWindow, QPushButton
from view.Handler.clicker_handler import OnClickHandler
from view.Widget.loading_screen import LoadingScreen
from view.UI_Files.main_window_ui import Ui_MainWindow
from view.Handler.signal_handler import OnSignalHandler
from view.Widget.slot_label import *


class MainWindow(QMainWindow):

    @property
    def ui(self):
        return self._ui

    def __init__(self, model, mc):
        super().__init__()

        self._model = model
        self._main_controller = mc
        self._ui = Ui_MainWindow()
        self._ui.setupUi(self)

        self.not_valid_slots = []
        self.end_loading = True

        self.current_problem_pathfile = ''
        self.save_state = True

        self.optimize_button = self.create_optimize_button()
        self._ui.central_widget.addWidget(self.optimize_button)
        self.optimize_button.hide()
        self.load = None

        self.ch = OnClickHandler(model, self, mc)
        self.sh = OnSignalHandler(model, self, mc)

        # connect widgets to controller
        self._ui.actionOpen_a_schedule.triggered.connect(self.ch.on_clicked_open_schedule)
        self._ui.actionAdd_teachers.triggered.connect(self.ch.on_teacher_add)
        self._ui.actionNew.triggered.connect(self.ch.on_clicked_new_schedule)
        self._ui.actionPeriod_edit.triggered.connect(self.ch.on_clicked_edit_period)
        self._ui.actionSauvegarder.triggered.connect(self.ch.on_clicked_save_problem)
        self.optimize_button.pressed.connect(self.ch.on_optimize_button_pressed)
        self._ui.actionExporter_en_format_LateX.triggered.connect(self.ch.on_clicked_latex)
        self._ui.actionEdit_le_score.triggered.connect(self.ch.on_clicked_score)
        self._ui.actionSauvegarder_sous.triggered.connect(self.ch.on_clicked_save_as_problem)
        # listen for model event signals
        self._model.schedule_name_changed.connect(self.sh.on_schedule_name_changed)
        self._model.problem_changed.connect(self.sh.on_problem_changed)
        self._main_controller.solver.solution_changed.connect(self.sh.on_solution_changed)
        self._main_controller.solver.solution_changed.connect(self._main_controller.on_solution_changed)
        self._main_controller.solver.error_pop.connect(self.on_error_popped)
        self._main_controller.solver.infeasible_error.connect(self.on_error_popped)

    @staticmethod
    def create_optimize_button():
        button = QPushButton("Optimiser l'horaire avec Horairyst")
        stylesheet = """QPushButton{{
                           font-size: {0};
                           color: {1};
                           background-color: {2};
                           border: 2px {4} {5};
                           padding: 4px;
                           border-radius: 8px;
                           }}""".format(qv.font_size_slot, qv.font_color_slot,
                                        qv.background_color_slot_full, qv.border_pixel_size_slot_full,
                                        qv.border_type_slot_empty, qv.border_color_slot_empty)
        hover = """QPushButton:hover{{
                       font-size: {0};
                       color: {1};
                       background-color: {2};
                       border:3px {4} {5};
                       }}""".format(qv.font_size_slot, qv.font_color_slot,
                                    qv.background_color_slot_full_hover,
                                    qv.border_pixel_size_slot_full_hover,
                                    qv.border_type_slot_empty, qv.border_color_slot_full)
        button.setStyleSheet(stylesheet + '\n' + hover)
        button.setMaximumWidth(300)
        button.setMinimumHeight(35)
        return button

    def add_waiting_list(self):
        if self._ui.waiting_layout.count() <= 0:
            for i in range(100):
                widget = QWaitingSlot("")
                widget.slot_changed.connect(self._main_controller.change_slot_pos)
                self._ui.waiting_layout.addWidget(widget)
        else:
            for i in reversed(range(self._ui.waiting_layout.count())):
                self._ui.waiting_layout.itemAt(i).widget().reset()

    def reset_view(self):
        for i in reversed(range(self._ui.gridLayout.count())):
            self._ui.gridLayout.itemAt(i).widget().setParent(None)

    @staticmethod
    def get_grid_index(i, j, period_len, session_len):
        padding = period_len + session_len + 1
        return padding + (i * session_len) + j

    def loading(self):
        if self.load is None:
            self.end_loading = False
            self.load = LoadingScreen(self, self._main_controller.solver.solverStarted)
        else:
            self.load.startAnimation()

    @pyqtSlot(str)
    def on_error_popped(self, e):
        self.end_loading = True
        msg = QMessageBox(self)
        msg.setIcon(QMessageBox.Critical)
        msg.setText(e)
        msg.setWindowTitle("Error")
        msg.exec_()

    def confirm_save(self):
        if not self.save_state:
            msg = QDialog(self)
            msg.setWindowTitle("Attention")
            label = QLabel(msg)
            label.setWordWrap(True)
            label.setText("Voulez-vous sauvegarder les dernières modifications ?")
            vbox = QVBoxLayout(msg)
            vbox.addWidget(label)
            buttonBox = QDialogButtonBox(msg)
            buttonBox.setOrientation(Qt.Horizontal)
            buttonBox.setStandardButtons(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
            buttonBox.accepted.connect(msg.accept)
            buttonBox.rejected.connect(msg.reject)
            vbox.addWidget(buttonBox)
            if msg.exec_():
                self.ch.on_clicked_save_problem()

    def correct_save(self):
        msg = QMessageBox(self)
        msg.setText("Votre horaire a correctement été sauvegardé !")
        msg.setWindowTitle("Sauvegarde effectuée")
        msg.exec_()

    def error_save(self):
        msg = QMessageBox(self)
        msg.setIcon(QMessageBox.Critical)
        msg.setText("Votre horaire n'a pas pu être sauvegardé !")
        msg.setWindowTitle("Erreur de sauvegarde")
        msg.exec_()

    def no_problem_open(self):
        msg = QMessageBox(self)
        msg.setIcon(QMessageBox.Critical)
        msg.setText("Aucun horaire n'est chargé. Veuillez créer ou charger un nouvel horaire.")
        msg.setWindowTitle("Pas d'horaire chargé")
        msg.exec_()
