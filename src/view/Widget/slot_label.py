import copy

import view.Widget.qss_values as qv
from controller import main_controller
from view.Handler.context_menu_handler import ContextMenuHandler
from view.Handler.double_click_handler import DoubleClickHandler
from view.Widget.q_abstract_slot import QAbstractSlot
from view.UI_Files.slot_edit_dialog import Ui_Dialog
from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QLabel, QDialog, QMessageBox, QCheckBox, QLineEdit, QDialogButtonBox, QVBoxLayout, QAction


class QSlot(QAbstractSlot, DoubleClickHandler):
    """
    Représentation d'un slot horaire sur l'ui.
    """

    MIN_HEIGHT = 80
    MAX_HEIGHT = 200

    MIN_WIDTH = 300
    MAX_WIDTH = 1920

    slot_changed = pyqtSignal(list)
    slot_created = pyqtSignal(list)
    slot_deleted = pyqtSignal(list)

    @property
    def validity(self):
        """
        Exprime la validité d'une case au niveau des conflits horaires.
        Si non valide, la case est représentée en rouge.
        :return: true si le slot est valide
        """
        return self._validity

    @validity.setter
    def validity(self, value):
        self._validity = value

    def __init__(self, i_pos, j_pos, student, d="", r="", t="Projet", title="", reasons=None):
        super().__init__(i_pos, j_pos, student, d, r, t, title, reasons)

        self.setMaximumWidth(self.MAX_WIDTH)
        self.setMinimumWidth(self.MIN_WIDTH)

        self.setMaximumHeight(self.MAX_HEIGHT)
        self.setMinimumHeight(self.MIN_HEIGHT)

        self._validity = True

    @pyqtSlot(int)
    def on_double_clicked(self, value):
        """
        Gère le double clique sur un slot.
        Ouvre une nouvelle fenêtre de dialogue permettant la modification des étudiants, professeurs,
        et type du slot.
        :param value: le nombre de clique sur la case
        :return: None
        """
        diag = QDialog()
        diag.setWindowTitle("Edition de la présentation")
        slot_edit = Ui_Dialog()
        slot_edit.setupUi(diag)
        for i, t in enumerate(main_controller.all_current_teacher):
            dir_box = QCheckBox(t)
            rap_box = QCheckBox(t)
            if t in self._dir:
                dir_box.setChecked(True)
            slot_edit.directors_layout.addWidget(dir_box, i / 3, i % 3, 1, 1)
            if t in self._rap:
                rap_box.setChecked(True)
            slot_edit.rapporters_layout.addWidget(rap_box, i / 3, i % 3, 1, 1)
        if self.text() != "":
            slot_edit.type_comboBox.setCurrentText(self._type)
            slot_edit.student_edit.setText(self._student)
        if diag.exec_():
            student = slot_edit.student_edit.text()
            d = []
            for i in range(slot_edit.directors_layout.count()):
                box = slot_edit.directors_layout.itemAt(i).widget()
                if box.isChecked():
                    d.append(box.text())
            r = []
            for i in range(slot_edit.directors_layout.count()):
                box = slot_edit.rapporters_layout.itemAt(i).widget()
                if box.isChecked():
                    r.append(box.text())
            print("director:", d)
            print("rapporter:", r)
            to_edit = self.check_edit([student, d, r])
            print(to_edit)
            if to_edit[0] != -1:
                self._type = slot_edit.type_comboBox.currentText()
                self._student = to_edit[0]
                self._dir = to_edit[1]
                self._rap = to_edit[2]
                self.slot_created.emit([self.i_pos, self.j_pos, self._student, self._dir, self._rap, self._type])
                self.refresh()
            else:
                self.diag_error(to_edit[1])

    def on_deleted_slot(self):
        self.slot_deleted.emit([self.i_pos, self.j_pos, self._student, self._dir, self._rap, self._type])

    @staticmethod
    def check_edit(l):
        """
        Vérifie les arguments entrés dans la fenêtre de dialogue de modification d'un slot.
        :param l: list, [etudiant, directeurs, rapporteur]
        :return: -1, msg_error si les arguments comportent des erreurs
                 l sinon
        """
        if l[0] == "":
            return -1, "Le champ 'Etudiant' est vide. Veuillez compléter le champ 'Etudiant'."
        if any(char.isdigit() for char in l[0]):
            return -1, "Des chiffres composent le nom de l'étudiant. Le nom d'un étudiant ne peut être composé de " \
                       "chiffres. "
        if l[1] == [""] or len(l[1]) == 0:
            return -1, "Il n'y a pas de directeur défini pour cette présentation. Un étudiant doit toujours avoir au " \
                       "minimum un directeur de Projet/Mémoire. "
        if l[2] == [""] or len(l[2]) == 0:
            return -1, "Il n'y a pas de rapproteur défini pour cette présentation. Un étudiant doit toujours avoir au " \
                       "minimum un rapporteur de Projet/Mémoire. "
        for d in l[1]:
            if any(char.isdigit() for char in d):
                return -1, "Des chiffres composent le nom d'un directeur. Le nom d'un directeur ne peut être composé " \
                           "de chiffres. "
            if d in l[2]:
                return -1, "Un professeur ne peut pas se retrouver à la fois directeur et rapporteur."
        for r in l[2]:
            if any(char.isdigit() for char in r):
                return -1, "Des chiffres composent le nom d'un rapporteur. Le nom d'un rapporteur ne peut être " \
                           "composé de chiffres. "
        return l

    def diag_error(self, err):
        """
        Ouvre une fenêtre d'erreur.
        :param err: str, le message d'erreur à afficher
        :return: None
        """
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setText(err)
        msg.setWindowTitle("Critical Error")
        msg.setIcon(QMessageBox.Critical)
        msg.setWindowFlags(Qt.WindowStaysOnTopHint)
        msg.exec_()

    def setStudent(self, student):
        self._student = student

    def setRapporters(self, r):
        if type(r) == str:
            if r.strip() != "":
                self._rap.append(r)
        else:
            self._rap = copy.deepcopy(r)

    def setDirectors(self, d):
        if type(d) == str:
            if d.strip() != "":
                self._dir.append(d)
        else:
            self._dir = copy.deepcopy(d)

    def setSubject(self, t):
        self._type = t

    def empty_reasons(self):
        self.reasons = []


class QWaitingSlot(QSlot):

    MIN_WIDTH = 200
    MAX_WIDTH = 250

    MIN_HEIGHT = 60
    MAX_HEIGHT = 75

    def __init__(self, student, d="", r="", t="Projet", title="Théorème Todo"):
        super().__init__(-1, -1, student, d, r, t, title)

        self.setMaximumWidth(self.MAX_WIDTH)
        self.setMinimumWidth(self.MIN_WIDTH)

        self.setMaximumHeight(self.MAX_HEIGHT)
        self.setMinimumHeight(self.MIN_HEIGHT)

        self.setAlignment(Qt.AlignLeft)
        self.setSlotText(self._student, self._dir, self._rap, self._type, self._title)
        self.setStyleSheet(QAbstractSlot.getSlotStyleSheet(self.text()))

    def setSlotText(self, _student, _dir, _rap, _type, _title, validity=True, reasons=None):
        text = ""
        # s'il n'y a pas de titre, considérer ça comme une présentation d'élève
        if _student.strip() != "":
            # s'il y a effectivement un étudiant, l'afficher
            text += "" + _type + " <b>|</b> " + _student + "<br> "

        if text != "" and len(_dir) > 0 or len(_rap) > 0:
            text += "<b>J:</b> "
            jury = copy.deepcopy(_dir)
            jury.extend(_rap)
            for i, d in enumerate(jury):
                if (i + 1) % 2 == 0 and i != 0:
                    text += d + "<br>"
                elif i == len(jury) - 1:
                    text += d
                else:
                    text += d + ", "
        self.setText(text)

    def refresh(self):
        self.setSlotText(self._student, self._dir, self._rap, self._type, self._title)
        self.setStyleSheet(QAbstractSlot.getSlotStyleSheet(self.text()))


class QSession(QLabel, ContextMenuHandler, DoubleClickHandler):

    MIN_HEIGHT = 30
    MAX_HEIGHT = 40

    session_added = pyqtSignal(list)
    session_changed = pyqtSignal(list)
    session_deleted = pyqtSignal(list)

    def __init__(self, session):
        super().__init__()
        self._session = session
        self.setText(self._session)
        self.setMaximumHeight(self.MAX_HEIGHT)
        self.setMinimumHeight(self.MIN_HEIGHT)
        self.setAlignment(Qt.AlignCenter)
        self.setAcceptDrops(True)

        add_action = QAction('Ajouter une session', self)
        add_action.triggered.connect(self.on_add_session)
        modify_action = QAction('Modifier la session', self)
        modify_action.triggered.connect(self.on_modify)
        delete_action = QAction('Supprimer la session', self)
        delete_action.triggered.connect(self.on_delete_session)
        self.popMenu.addAction(add_action)
        self.popMenu.addAction(modify_action)
        self.popMenu.addSeparator()
        self.popMenu.addAction(delete_action)

        self.setStyleSheet("""QLabel{{
                               font-size: {0} pt;
                               color: {1};
                               background-color: {2};
                               border: {3}px {4} {5};
                               }}""".format(qv.font_size_session, qv.font_color_session,
                                            qv.background_color_session, qv.border_pixel_size_session,
                                            qv.border_type_session, qv.border_color_session))

    def on_add_session(self):
        self.session_added.emit([self.text()])

    def on_delete_session(self):
        self.session_deleted.emit([self.text()])

    def on_modify(self):
        self.on_double_clicked(2)

    @pyqtSlot(int)
    def on_double_clicked(self, value):
        # creation de la fenetre
        diag = QDialog(self)
        diag.setWindowTitle("Edition d'une session")
        diag.setFixedSize(200, 100)
        session_edit = QLineEdit(diag)
        session_edit.setText(self.text())
        buttonBox = QDialogButtonBox(diag)
        buttonBox.setOrientation(Qt.Horizontal)
        buttonBox.setStandardButtons(QDialogButtonBox.Cancel | QDialogButtonBox.Ok)
        buttonBox.accepted.connect(diag.accept)
        buttonBox.rejected.connect(diag.reject)

        verticalLayout = QVBoxLayout(diag)
        verticalLayout.addWidget(session_edit)
        verticalLayout.addWidget(buttonBox)

        if diag.exec_():
            self.session_changed.emit([[self.text()], [session_edit.text()]])
            self.setText(session_edit.text())

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.left_click_count += 1
            if not self.timer.isActive():
                self.timer.start()


class QHour(QLabel):

    MIN_WIDTH = 60
    MAX_WIDTH = 70

    def __init__(self, hour):
        super().__init__()
        self._hour = hour
        self.setText(self._hour)
        self.setAlignment(Qt.AlignCenter)
        self.setMinimumWidth(self.MIN_WIDTH)
        self.setMaximumWidth(self.MAX_WIDTH)
        self.setMargin(0)
        self.setStyleSheet("""QLabel{{
                               font-size: {0} pt;
                               color: {1};
                               background-color: {2};
                               border: {3}px {4} {5};
                               }}""".format(qv.font_size_period, qv.font_color_period,
                                            qv.background_color_period, qv.border_pixel_size_period,
                                            qv.border_type_period, qv.border_color_period))


class QEmpty(QLabel):

    def __init__(self):
        super().__init__()
        self.setText("")
        self.setStyleSheet("""QLabel{
                            background-color:rgb(236, 240, 241);
                            border: 1px solid rgb(44, 62, 80);
                            }""")
