import copy

from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, QMimeData, pyqtSlot
from PyQt5.QtGui import QDrag, QPixmap, QPainter
from PyQt5.QtWidgets import QLabel, QApplication
from view.Widget import qss_values as qv
from view.Handler.context_menu_handler import ContextMenuHandler


class QAbstractSlot(QLabel, ContextMenuHandler):

    def __init__(self, i_pos, j_pos, student, d="", r="", t="Projet", title="", reasons=None):
        super().__init__()

        if reasons is None:
            reasons = []

        self.drag_start_position = None

        self.i_pos = i_pos
        self.j_pos = j_pos

        self._title = title
        self._student = student
        # l'ensemble des professeurs accessible dans l'horaire chargé
        # utilisé dans la création/modification d'un slot
        self._teachers = []

        self._dir, self._rap = [], []

        self.setDirectors(d)
        self.setRapporters(r)
        self._type = t
        self.reasons = reasons

        # début de la complétion du texte
        self.setText(self.setSlotText(self._student, self._dir, self._rap, self._type, self._title))
        self.setAlignment(Qt.AlignCenter)
        self.setStyleSheet(self.getSlotStyleSheet(self.text()))

        self.setAcceptDrops(True)

        modify_action = QtWidgets.QAction('Ajouter/Modifier une présentation', self)
        delete_action = QtWidgets.QAction('Supprimer la présentation', self)
        modify_action.triggered.connect(self.on_action_modify)
        delete_action.triggered.connect(self.on_action_deleted)

        self.popMenu.addAction(modify_action)
        self.popMenu.addSeparator()
        self.popMenu.addAction(delete_action)

    @pyqtSlot(bool)
    def on_action_modify(self, value):
        self.on_double_clicked(2)

    @pyqtSlot(bool)
    def on_action_deleted(self, value):
        self.on_deleted_slot()

    def setSlotText(self, _student, _dir, _rap, _type, _title, validity=True, reasons=None):
        """
        Construit une chaîne de caractère formatée pour l'affichage d'un slot horaire.
        :param _student: str, l'étudiant qui présente son examen
        :param _dir: lst, la liste des directeurs
        :param _rap: lst, la liste des rapporteurs
        :param _type: le type de présentation
        :param _title: str, le titre de la présentation (optionnel)
        :param validity: boolean, la validité de l'horaire (par défaut True)
        :param reasons: lst, la liste des raisons de la non-validité du slot horaire
        :return: None
        """
        if reasons is None:
            reasons = []
        text = ""
        if _title.strip() == "":
            # s'il n'y a pas de titre, considérer ça comme une présentation d'élève
            if _student.strip() != "":
                # s'il y a effectivement un étudiant, l'afficher
                if _type == "Projet":
                    text += "<b style=\"color:#FFFFFF;font-size:18px\">"
                else:
                    text += "<b style=\"color:#FFFFFF;font-size:18px\">"
                text += _type + "</b><br>présenté par <b>" + _student + "</b>"
            # sinon ne rien faire car case vide
        else:
            # s'il y a titre
            if _type.strip() != "" and _student.strip() != "":
                # avec un étudiant et un type, c'est une présentation avec titre
                text += "<i>" + _type + ":</i> "
                text += "<u>" + _title + "</u><br>"
                text += "de <b>" + _student + "</b>"
            elif _type == "special_type":
                # avec un type spécial, alors on affiche le titre en couleur
                text += "<span style=\" color:#27ae60;\">" + _title + "</span><br>"
                text += "Participants: " + _student + "</b>"
        if text != "" and len(_dir) > 0 or len(_rap) > 0:
            text += "<br>Jury: "
            jury = copy.deepcopy(_dir)
            jury.extend(_rap)
            for i, d in enumerate(jury):
                if d in reasons:
                    d = '<s>' + d + '</s>'
                if (i + 1) % 2 == 0 and i != len(jury) - 1:
                    text += "<b>" + d + "</b> <br>"
                elif i == len(jury) - 1:
                    text += "<b>" + d + "</b>"
                else:
                    text += "<b>" + d + "</b>, "
        if validity:
            return text
        else:
            tail = '<span style="text-align:left; font-size:10px">Conflits: '
            already_added = []
            i = 0
            for r in reasons:
                if r not in already_added:
                    already_added.append(r)
                    r = self.format_conflicts(r)
                    if (i + 1) % 2 == 0 and i != len(reasons) - 1:
                        tail += "<b>" + r + "</b> <br>"
                    elif i == len(reasons) - 1:
                        tail += "<b>" + r + "</b></span>"
                    else:
                        tail += "<b>" + r + "</b>, "
                    i += 1
            return text

    @staticmethod
    def format_conflicts(conflict):
        return conflict

    @staticmethod
    def getSlotStyleSheet(text, validity=True):
        """
        Construit la feuille de style du slot en fonction du texte de ce dernier
        :param text: Le texte à prendre en compte pour la construction de la feuille de style
        :param validity: La validité du slot
        :return: str, la feuille de style du QWidget QSlot
        """
        if not validity:
            stylesheet = """QLabel{{
                               font-size: {0};
                               color: {1};
                               background-color: {2};
                               border: {3}px {4} {5};
                               }}""".format(qv.font_size_slot, qv.font_color_slot,
                                            qv.background_color_slot_not_valid, qv.border_pixel_size_slot_full,
                                            qv.border_type_slot_empty, qv.border_color_slot_empty)
            hover = """QLabel:hover{{
                           font-size: {0};
                           color: {1};
                           background-color: {2};
                           border: {3}px {4} {5};
                           }}""".format(qv.font_size_slot, qv.font_color_slot,
                                        qv.background_color_slot_not_valid_hover,
                                        qv.border_pixel_size_slot_full_hover,
                                        qv.border_type_slot_full, qv.border_color_slot_full)
            return stylesheet + "\n" + hover
        if text.strip() == "":
            stylesheet = """QLabel{{
                               background-color: {0};
                               border: {1}px {2} {3};
                               }}""".format(qv.background_color_slot_empty, qv.border_pixel_size_slot_empty,
                                            qv.border_type_slot_empty, qv.border_color_slot_empty)
            hover = """QLabel:hover{{
                            border: {0}px {1} {2};
                            background-color: {3};
                        }}
                        """.format(qv.border_pixel_size_slot_empty_hover,
                                   qv.border_type_slot_hover,
                                   qv.border_color_slot_empty,
                                   qv.background_color_slot_empty_hover)
        else:
            stylesheet = """QLabel{{
                               font-size: {0};
                               color: {1};
                               background-color: {2};
                               border: {3}px {4} {5};
                               }}""".format(qv.font_size_slot, qv.font_color_slot,
                                            qv.background_color_slot_full, qv.border_pixel_size_slot_full,
                                            qv.border_type_slot_empty, qv.border_color_slot_empty)
            hover = """QLabel:hover{{
                           font-size: {0};
                           color: {1};
                           background-color: {2};
                           border: {3}px {4} {5};
                           }}""".format(qv.font_size_slot, qv.font_color_slot,
                                        qv.background_color_slot_full_hover, qv.border_pixel_size_slot_full_hover,
                                        qv.border_type_slot_empty, qv.border_color_slot_full)

        return stylesheet + "\n" + hover

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.drag_start_position = event.pos()
        if event.button() == Qt.LeftButton:
            self.left_click_count += 1
            if not self.timer.isActive():
                self.timer.start()

    def mouseMoveEvent(self, event):
        if not (event.buttons() & Qt.LeftButton):
            return
        if self.text() == "":
            return
        if (event.pos() - self.drag_start_position).manhattanLength() < QApplication.startDragDistance():
            return
        drag = QDrag(self)
        mimedata = QMimeData()
        mimedata.setText(self.text())
        mimedata.setProperty("student", self._student)
        mimedata.setProperty("type", self._type)
        mimedata.setProperty("director", self._dir)
        mimedata.setProperty("rapporter", self._rap)
        mimedata.setProperty("title", self._title)
        mimedata.setProperty("source", self)
        drag.setMimeData(mimedata)
        pixmap = QPixmap(self.size())
        painter = QPainter(pixmap)
        painter.drawPixmap(self.rect(), self.grab())
        painter.end()
        drag.setPixmap(pixmap)
        drag.setHotSpot(event.pos())

        self.reset()
        self.setStyleSheet(self.getSlotStyleSheet(self.text()))
        drag.exec_(Qt.CopyAction | Qt.MoveAction)

    def dragEnterEvent(self, event):
        if event.mimeData().hasText():
            event.acceptProposedAction()
            if self.text() == "":
                addon = """QLabel{{
                   background-color: {0};
                   border: {1}px {2} {3};
                   }}""".format(qv.background_color_slot_empty, qv.border_pixel_size_slot_drag_hover,
                                qv.border_type_slot_empty, qv.border_color_slot_hover_empty)
            else:
                addon = """
                            QLabel{{
                                border: {0}px {1} {2};
                            }}
                            """.format(qv.border_pixel_size_slot_drag_hover,
                                       qv.border_type_slot_hover,
                                       qv.border_color_slot_hover_full)

            self.setStyleSheet(self.getSlotStyleSheet(self.text())
                               + addon)

    def dragLeaveEvent(self, event):
        self.setStyleSheet(self.getSlotStyleSheet(self.text()))

    def dropEvent(self, event):
        if self.text() == "":
            text = event.mimeData().text()
            self.setText(text)
            event.acceptProposedAction()
            source = event.mimeData().property("source")
            toEmit = [self.i_pos, self.j_pos, source.i_pos, source.j_pos, event.mimeData()]
            self.slot_changed.emit(toEmit)
            self._title = event.mimeData().property("title")
            self._student = event.mimeData().property("student")
            self._dir = event.mimeData().property("director")
            self._rap = event.mimeData().property("rapporter")
            self._type = event.mimeData().property("type")
            self.refresh()
        else:
            text = event.mimeData().text()
            source = event.mimeData().property("source")
            source.setText(text)
            source.setStyleSheet(self.getSlotStyleSheet(text))
            source._title = event.mimeData().property("title")
            source._student = event.mimeData().property("student")
            source._dir = event.mimeData().property("director")
            source._rap = event.mimeData().property("rapporter")
            source._type = event.mimeData().property("type")
            event.acceptProposedAction()
            self.refresh()

    def refresh(self):
        """
        Rafraichit le contenu du slot. Permet de mettre à jour la feuille de style.
        :return: None
        """
        self.setText(self.setSlotText(self._student, self._dir, self._rap, self._type, self._title,
                                      validity=self.validity,
                                      reasons=self.reasons))
        self.setStyleSheet(self.getSlotStyleSheet(self.text(), self.validity))

    def reset(self):
        """
        Reset le texte du slot. Supprime toute les données sauvegardées à l'intérieur de l'objet.
        :return:
        """
        self.setText("")
        self._title = ""
        self._dir = []
        self._rap = []
        self._type = ""
        self._student = ""
