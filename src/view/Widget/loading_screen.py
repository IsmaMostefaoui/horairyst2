from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtGui import QMovie
from PyQt5.QtWidgets import QWidget, QLabel

from horairyst.problem.problem import Problem


class LoadingScreen(QWidget):

    def __init__(self, parent, stop_signal):
        super().__init__(parent)
        self.setFixedSize(200, 200)
        self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.CustomizeWindowHint)
        self.move(int(parent.width()/2-self.width()/2), int(parent.height()/2-self.height()/2))
        self.label_loading = QLabel(self)
        self.movie = QMovie('ressources/images/Loading_1.gif')
        self.label_loading.setMovie(self.movie)
        self.startAnimation()
        stop_signal.connect(self.stopAnimation)

    def startAnimation(self):
        self.movie.start()
        self.show()

    @pyqtSlot()
    def stopAnimation(self):
        self.movie.stop()
        self.stop_loading_bool = False
        self.close()
