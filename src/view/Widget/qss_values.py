background_color_slot_empty = "rgba(127, 140, 141, 150)"
background_color_slot_full = "rgb(127, 140, 141)"
background_color_session = "rgb(83, 92, 104)"
background_color_period = "rgb(83, 92, 104,)"
background_color_slot_empty_hover = "rgba(127, 140, 141, 200)"
background_color_slot_full_hover = "rgb(117, 130, 131)"
background_color_slot_not_valid = "rgb(192, 57, 43)"
background_color_slot_not_valid_hover = "rgb(182, 47, 33)"

border_color_slot_empty = "rgba(44, 62, 80, 200)"
border_color_slot_full = "rgb(44, 62, 80)"
border_color_slot_hover_empty = "rgb(39, 174, 96)"
border_color_slot_hover_full = "rgb(194, 80, 50)"
border_color_session = "rgb(44, 62, 80)"
border_color_period = "rgb(44, 62, 80)"

border_type_slot_empty = "solid"
border_type_slot_full = "solid"
border_type_slot_hover = "solid"
border_type_session = "solid"
border_type_period = "solid"

border_pixel_size_slot_drag_hover = 3
border_pixel_size_slot_empty_hover = 2
border_pixel_size_slot_full_hover = 2
border_pixel_size_slot_full = 1
border_pixel_size_slot_empty = 1
border_pixel_size_session = 1
border_pixel_size_period = 1

font_size_slot = 15
font_size_session = 15
font_size_period = 15

font_color_slot = "white"
font_color_session = "white"
font_color_period = "white"
font_color_project = "rgb(41, 128, 185)"
font_color_memoire = "rgb(39, 174, 96)"
