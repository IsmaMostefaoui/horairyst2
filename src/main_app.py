import sys
from PyQt5.QtWidgets import QApplication
from model.main_loader import ModelLoader
from controller.main_controller import MainController
from view.main_window import MainWindow


class App(QApplication):
    def __init__(self, sys_argv):
        super(App, self).__init__(sys_argv)
        self.model = ModelLoader()
        self.main_controller = MainController(self.model)
        self.main_view = MainWindow(self.model, self.main_controller)
        self.main_view.show()


if __name__ == "__main__":
    app = App(sys.argv)
    sys.exit(app.exec_())
